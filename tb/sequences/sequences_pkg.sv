// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

`include <uvm_macros.svh>

package sequences_pkg;
  import one_wire_agent_pkg::*;
  import one_wire_master_env_pkg::*;
  import uvm_pkg::*;

  `include "one_wire_bus_reset_sequence.svh"
  `include "one_wire_touch_bit_sequence.svh"
  `include "one_wire_write_bit_sequence.svh"
  `include "one_wire_read_bit_sequence.svh"
  `include "one_wire_touch_byte_sequence.svh"
  `include "one_wire_write_byte_sequence.svh"
  `include "one_wire_read_byte_sequence.svh"
  `include "one_wire_triplet_sequence.svh"
endpackage
