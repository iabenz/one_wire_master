// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_touch_byte_sequence extends uvm_reg_sequence;
  `uvm_object_utils(one_wire_touch_byte_sequence)

  bit [7:0] clk_div;
  rand byte write;
  rand byte read;
  rand bit overdrive;
  rand bit spu;
  rand bit apu;
  rand bit irq_en;

  function new(string name = "one_wire_touch_byte_sequence");
    super.new(name);
  endfunction

  virtual task body();
    one_wire_master_operation_sequence run_operation =
        one_wire_master_operation_sequence::type_id::create("run_operation");
    one_wire_byte_sequence one_wire_byte =
        one_wire_byte_sequence::type_id::create("one_wire_byte");

    run_operation.model = model;
    run_operation.clk_div = clk_div;
    run_operation.param = write;
    run_operation.operation = one_wire_master_write_byte_operation;
    run_operation.overdrive = overdrive;
    run_operation.spu = spu;
    run_operation.apu = apu;
    run_operation.irq_en = irq_en;
    run_operation.result = write & read;

    one_wire_byte.data = read;
    one_wire_byte.overdrive = overdrive;

    fork
      run_operation.start(null, this);
      one_wire_byte.start(get_sequencer(), this);
    join
  endtask
endclass
