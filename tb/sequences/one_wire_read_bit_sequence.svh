// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_read_bit_sequence extends uvm_reg_sequence;
  `uvm_object_utils(one_wire_read_bit_sequence)

  bit [7:0] clk_div;
  rand bit data;
  rand bit overdrive;
  rand bit spu;
  rand bit apu;
  rand bit irq_en;

  function new(string name = "one_wire_read_bit_sequence");
    super.new(name);
  endfunction

  virtual task body();
    one_wire_touch_bit_sequence touch_bit =
        one_wire_touch_bit_sequence::type_id::create("touch_bit");
    touch_bit.model = model;
    touch_bit.clk_div = clk_div;
    touch_bit.write = '1;
    touch_bit.read = data;
    touch_bit.overdrive = overdrive;
    touch_bit.spu = spu;
    touch_bit.apu = apu;
    touch_bit.irq_en = irq_en;
    touch_bit.start(get_sequencer(), this);
  endtask
endclass
