// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class apb_master_driver extends uvm_driver#(apb_sequence_item);
  `uvm_component_utils(apb_master_driver)

  virtual apb_agent_if vif;
  realtime pclk_period;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual task run_phase(uvm_phase phase);
    super.run_phase(phase);

    vif.preset_n <= 0;
    wait(pclk_period > 0) #pclk_period vif.preset_n <= 1;

    fork
      begin
        vif.pclk <= 0;
        forever wait(pclk_period > 0) #(pclk_period / 2) vif.pclk <= ~vif.pclk;
      end

      begin
        enum { idle, setup, access } state = idle;
        apb_sequence_item seq_item;

        forever begin
          case (state)
            idle: begin
              vif.psel <= 0;
              vif.penable <= 0;
              seq_item_port.get_next_item(seq_item);
              @(posedge vif.pclk);
              state = setup;
            end

            setup: begin
              vif.psel <= 1;
              vif.penable <= 0;
              vif.paddr <= seq_item.addr;
              vif.pwrite <= seq_item.write;
              if (seq_item.write) begin
                vif.pwdata <= seq_item.data;
              end
              @(posedge vif.pclk);
              state = access;
            end

            access: begin
              vif.psel <= 1;
              vif.penable <= 1;
              @(posedge vif.pclk);
              if (vif.pready) begin
                if (!seq_item.write) begin
                  if ($isunknown(vif.prdata)) begin
                    `uvm_error(get_type_name(), "PRDATA is unknown")
                  end else begin
                    seq_item.data = vif.prdata;
                  end
                end
                seq_item_port.item_done();
                seq_item_port.try_next_item(seq_item);
                if (seq_item == null) begin
                  state = idle;
                end else begin
                  state = setup;
                end
              end else if ($isunknown(vif.pready)) begin
                `uvm_error(get_type_name(), "PREADY is unknown")
              end
            end
          endcase
        end
      end
    join
  endtask
endclass
