// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class apb_monitor extends uvm_monitor;
  `uvm_component_utils(apb_monitor)

  virtual apb_agent_if vif;
  uvm_analysis_port#(apb_sequence_item) seq_item_port;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    seq_item_port = new("seq_item_port", this);
  endfunction

  virtual task run_phase(uvm_phase phase);
    super.run_phase(phase);
    forever @(posedge vif.pclk) begin
      // Wait for PRESETn, PSEL, PENABLE, and PREADY to be set.
      if (vif.preset_n) begin
        if (vif.psel && vif.penable) begin
          if (vif.pready) begin
            apb_sequence_item seq_item =
                apb_sequence_item::type_id::create("seq_item");
            bit seq_item_valid = 1;

            if ($isunknown(vif.paddr)) begin
              `uvm_error(get_type_name(), "PADDR is unknown")
              seq_item_valid = 0;
            end else begin
              seq_item.addr = vif.paddr;
            end

            if ($isunknown(vif.pwrite)) begin
              `uvm_error(get_type_name(), "PWRITE is unknown")
              seq_item_valid = 0;
            end else begin
              seq_item.write = vif.pwrite;

              if (vif.pwrite) begin
                if ($isunknown(vif.pwdata)) begin
                  `uvm_error(get_type_name(), "PWDATA is unknown")
                  seq_item_valid = 0;
                end else begin
                  seq_item.data = vif.pwdata;
                end
              end else begin
                if ($isunknown(vif.prdata)) begin
                  `uvm_error(get_type_name(), "PRDATA is unknown")
                  seq_item_valid = 0;
                end else begin
                  seq_item.data = vif.prdata;
                end
              end
            end

            if (seq_item_valid) begin
              seq_item_port.write(seq_item);
            end
          end else if ($isunknown(vif.pready)) begin
            `uvm_error(get_type_name(), "PREADY is unknown")
          end
        end else begin
          if ($isunknown(vif.psel)) begin
            `uvm_error(get_type_name(), "PSEL is unknown")
          end

          if ($isunknown(vif.penable)) begin
            `uvm_error(get_type_name(), "PENABLE is unknown")
          end
        end
      end else if ($isunknown(vif.preset_n)) begin
        `uvm_error(get_type_name(), "PRESETn is unknown")
      end
    end
  endtask
endclass
