// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class apb_reg_adapter extends uvm_reg_adapter;
  `uvm_object_utils(apb_reg_adapter)

  function new(string name = "apb_reg_adapter");
    super.new(name);
    supports_byte_enable = 0;
    provides_responses = 0;
  endfunction

  virtual function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw);
    apb_sequence_item seq_item = apb_sequence_item::type_id::create("seq_item");
    seq_item.addr = rw.addr;
    seq_item.write = rw.kind == UVM_WRITE;
    seq_item.data = rw.data;
    return seq_item;
  endfunction

  virtual function void bus2reg(uvm_sequence_item bus_item,
                                ref uvm_reg_bus_op rw);
    apb_sequence_item seq_item;
    if (!$cast(seq_item, bus_item)) begin
      `uvm_fatal(get_type_name(), "Failed to cast bus_item to sequence_item")
    end
    rw.kind = seq_item.write ? UVM_WRITE : UVM_READ;
    rw.addr = seq_item.addr;
    rw.data = seq_item.data;
    rw.status = UVM_IS_OK;
  endfunction
endclass
