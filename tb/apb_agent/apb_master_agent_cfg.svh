// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class apb_master_agent_cfg extends uvm_object;
  `uvm_object_utils(apb_master_agent_cfg)

  virtual apb_agent_if vif;
  realtime pclk_period;

  function new(string name = "apb_master_agent_cfg");
    super.new(name);
  endfunction
endclass
