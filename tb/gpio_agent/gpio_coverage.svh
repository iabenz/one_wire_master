// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class gpio_coverage extends uvm_subscriber#(gpio_sequence_item);
  `uvm_component_utils(gpio_coverage)

  covergroup cg with function sample(bit value);
    option.per_instance = 1;
    coverpoint value;
  endgroup

  function new(string name, uvm_component parent);
    super.new(name, parent);
    cg = new;
    cg.set_inst_name(get_full_name());
  endfunction

  virtual function void write(T t);
    cg.sample(t.value);
  endfunction
endclass
