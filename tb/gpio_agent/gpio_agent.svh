// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class gpio_agent extends uvm_agent;
  `uvm_component_utils(gpio_agent)

  uvm_sequencer#(gpio_sequence_item) sequencer;
  gpio_driver driver;
  gpio_monitor monitor;
  gpio_coverage coverage;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    gpio_agent_cfg cfg;

    if (!uvm_config_db#(gpio_agent_cfg)::get(this, "", "cfg", cfg)) begin
      `uvm_fatal(get_type_name(), "Failed to get configuration (cfg)")
    end

    super.build_phase(phase);

    if (get_is_active() == UVM_ACTIVE) begin
      sequencer = uvm_sequencer#(gpio_sequence_item)::type_id::create(
          "sequencer", this);

      driver = gpio_driver::type_id::create("driver", this);
      driver.vif = cfg.vif;
    end

    monitor = gpio_monitor::type_id::create("monitor", this);
    monitor.vif = cfg.vif;

    if (cfg.coverage) begin
      coverage = gpio_coverage::type_id::create("coverage", this);
    end
  endfunction

  virtual function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);

    if (get_is_active() == UVM_ACTIVE) begin
      driver.seq_item_port.connect(sequencer.seq_item_export);
    end

    if (coverage != null) begin
      monitor.seq_item_port.connect(coverage.analysis_export);
    end
  endfunction
endclass
