// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class gpio_monitor extends uvm_monitor;
  `uvm_component_utils(gpio_monitor)

  virtual gpio_agent_if vif;
  uvm_analysis_port#(gpio_sequence_item) seq_item_port;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    seq_item_port = new("seq_item_port", this);
  endfunction

  virtual task run_phase(uvm_phase phase);
    super.run_phase(phase);
    forever @(vif.value) begin
      if ($isunknown(vif.value)) begin
        `uvm_error(get_type_name(), "GPIO is unknown")
      end else begin
        gpio_sequence_item seq_item =
            gpio_sequence_item::type_id::create("seq_item");
        seq_item.value = vif.value;
        seq_item_port.write(seq_item);
      end
    end
  endtask
endclass
