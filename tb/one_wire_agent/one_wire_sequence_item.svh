// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_sequence_item extends uvm_sequence_item;
  rand bit reset;
  rand bit data;
  rand bit overdrive;

  `uvm_object_utils_begin(one_wire_sequence_item)
    `uvm_field_int(reset, UVM_DEFAULT)
    `uvm_field_int(data, UVM_DEFAULT)
    `uvm_field_int(overdrive, UVM_DEFAULT)
  `uvm_object_utils_end

  function new(string name = "one_wire_sequence_item");
    super.new(name);
  endfunction
endclass
