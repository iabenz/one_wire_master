// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_slave_agent extends uvm_agent;
  `uvm_component_utils(one_wire_slave_agent)

  uvm_sequencer#(one_wire_sequence_item) sequencer;
  one_wire_slave_driver driver;
  one_wire_monitor monitor;

  one_wire_coverage sample_coverage;
  gpio_coverage drive_coverage;
  gpio_coverage spu_coverage;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    one_wire_slave_agent_cfg cfg;

    if (!uvm_config_db#(one_wire_slave_agent_cfg)::get(this, "", "cfg",
                                                       cfg)) begin
      `uvm_fatal(get_type_name(), "Failed to get configuration (cfg)")
    end

    super.build_phase(phase);

    if (get_is_active() == UVM_ACTIVE) begin
      sequencer = uvm_sequencer#(one_wire_sequence_item)::type_id::create(
          "sequencer", this);

      driver = one_wire_slave_driver::type_id::create("driver", this);
      driver.vif = cfg.vif;
    end

    monitor = one_wire_monitor::type_id::create("monitor", this);
    monitor.vif = cfg.vif;

    if (cfg.coverage) begin
      sample_coverage =
          one_wire_coverage::type_id::create("sample_coverage", this);
      drive_coverage = gpio_coverage::type_id::create("drive_coverage", this);
      spu_coverage = gpio_coverage::type_id::create("spu_coverage", this);
    end
  endfunction

  virtual function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);

    if (get_is_active() == UVM_ACTIVE) begin
      driver.seq_item_port.connect(sequencer.seq_item_export);
    end

    if (sample_coverage != null) begin
      monitor.sample_port.connect(sample_coverage.analysis_export);
      monitor.drive_port.connect(drive_coverage.analysis_export);
      monitor.spu_port.connect(spu_coverage.analysis_export);
    end
  endfunction
endclass
