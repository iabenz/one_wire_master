// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_reset_sequence extends uvm_sequence#(one_wire_sequence_item);
  `uvm_object_utils(one_wire_reset_sequence)

  rand bit presence;
  rand bit overdrive;

  function new(string name = "one_wire_reset_sequence");
    super.new(name);
  endfunction

  virtual task body();
    one_wire_sequence_item item =
        one_wire_sequence_item::type_id::create("item");
    start_item(item);
    item.reset = 1;
    item.data = presence;
    item.overdrive = overdrive;
    finish_item(item);
  endtask
endclass
