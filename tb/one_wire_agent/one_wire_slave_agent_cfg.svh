// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_slave_agent_cfg extends uvm_object;
  `uvm_object_utils(one_wire_slave_agent_cfg)

  bit coverage = 1;
  virtual one_wire_agent_if vif;

  function new(string name = "one_wire_slave_agent_cfg");
    super.new(name);
  endfunction
endclass
