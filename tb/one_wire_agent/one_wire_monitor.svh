// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_monitor extends uvm_monitor;
  `uvm_component_utils(one_wire_monitor)

  virtual one_wire_agent_if vif;
  uvm_analysis_port#(one_wire_sequence_item) sample_port;
  uvm_analysis_port#(gpio_sequence_item) drive_port;
  uvm_analysis_port#(gpio_sequence_item) spu_port;
  bit overdrive;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    sample_port = new("sample_port", this);
    drive_port = new("drive_port", this);
    spu_port = new("spu_port", this);
  endfunction

  virtual task run_phase(uvm_phase phase);
    super.run_phase(phase);
    fork
      forever @(vif.sample) begin
        if ($isunknown(vif.sample)) begin
          `uvm_error(get_type_name(), "Sample is unknown")
        end
      end

      forever begin
        one_wire_sequence_item seq_item =
            one_wire_sequence_item::type_id::create("seq_item");
        time low_time;

        @(negedge vif.sample) low_time = $time;
        @(posedge vif.sample) low_time = $time - low_time;
        if (low_time >= (overdrive ? 48us : 480us)) begin
          seq_item.reset = 1;
          #(overdrive ? 8us : 68us) seq_item.data = ~vif.sample;
        end else if (low_time >= (overdrive ? 6us : 60us)) begin
          seq_item.reset = 0;
          seq_item.data = 0;
        end else begin
          seq_item.reset = 0;
          seq_item.data = 1;
        end
        seq_item.overdrive = overdrive;
        sample_port.write(seq_item);
      end

      forever @(vif.drive) begin
        if ($isunknown(vif.drive)) begin
          `uvm_error(get_type_name(), "Drive is unknown")
        end else begin
          gpio_sequence_item seq_item =
              gpio_sequence_item::type_id::create("seq_item");
          seq_item.value = vif.drive;
          drive_port.write(seq_item);
        end
      end

      forever @(vif.spu) begin
        if ($isunknown(vif.spu)) begin
          `uvm_error(get_type_name(), "SPU is unknown")
        end else begin
          gpio_sequence_item seq_item =
              gpio_sequence_item::type_id::create("seq_item");
          seq_item.value = vif.spu;
          spu_port.write(seq_item);
        end
      end
    join
  endtask
endclass
