// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_slave_driver extends uvm_driver#(one_wire_sequence_item);
  `uvm_component_utils(one_wire_slave_driver)

  virtual one_wire_agent_if vif;
  local bit drive;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  local function void update_sample(bit drive);
    logic sample = 'x;
    if ((drive || vif.drive) && !vif.spu) begin
      sample = '0;
    end else if (!drive && !vif.drive) begin
      sample = '1;
    end
    vif.sample <= sample;
    vif.apu <= sample;
  endfunction

  function bit get_drive();
    return drive;
  endfunction

  local function void set_drive(bit drive);
    this.drive <= drive;
    update_sample(drive);
  endfunction

  virtual task run_phase(uvm_phase phase);
    super.run_phase(phase);
    fork
      forever @(vif.drive or vif.spu) update_sample(drive);

      forever begin
        one_wire_sequence_item seq_item;
        seq_item_port.get_next_item(seq_item);
        wait(vif.sample);
        @(negedge vif.sample);
        if (seq_item.reset) begin
          if (seq_item.data) begin
            @(posedge vif.sample) #(seq_item.overdrive ? 4us : 40us)
                set_drive(1);
            #(seq_item.overdrive ? 16us : 150us) set_drive(0);
          end
        end else begin
          if (!seq_item.data) begin
            set_drive(1);
            #(seq_item.overdrive ? 8us : 64us) set_drive(0);
          end
        end
        seq_item_port.item_done();
      end
    join
  endtask
endclass
