// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_env extends uvm_env;
  `uvm_component_utils(one_wire_master_env)

  apb_master_agent apb_agent;
  gpio_agent irq_agent;
  one_wire_slave_agent one_wire_agent;
  one_wire_master_scoreboard scoreboard;
  one_wire_master_reg_block reg_block;
  apb_reg_adapter reg_adapter;
  uvm_reg_predictor#(apb_sequence_item) reg_predictor;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    one_wire_master_env_cfg cfg;
    apb_master_agent_cfg apb_agent_cfg;
    gpio_agent_cfg irq_agent_cfg;
    one_wire_slave_agent_cfg one_wire_agent_cfg;

    if (!uvm_config_db#(one_wire_master_env_cfg)::get(this, "", "cfg",
                                                      cfg)) begin
      `uvm_fatal(get_type_name(), "Failed to get configuration (cfg)")
    end

    super.build_phase(phase);

    uvm_config_int::set(this, "apb_agent", "is_active", cfg.apb_active);
    apb_agent_cfg = apb_master_agent_cfg::type_id::create("apb_agent_cfg");
    apb_agent_cfg.vif = cfg.apb_vif;
    apb_agent_cfg.pclk_period = cfg.apb_pclk_period;
    uvm_config_db#(apb_master_agent_cfg)::set(this, "apb_agent", "cfg",
                                              apb_agent_cfg);
    apb_agent = apb_master_agent::type_id::create("apb_agent", this);

    uvm_config_int::set(this, "irq_agent", "is_active", UVM_PASSIVE);
    irq_agent_cfg = gpio_agent_cfg::type_id::create("irq_agent_cfg");
    irq_agent_cfg.coverage = cfg.irq_coverage;
    irq_agent_cfg.vif = cfg.irq_vif;
    uvm_config_db#(gpio_agent_cfg)::set(this, "irq_agent", "cfg",
                                        irq_agent_cfg);
    irq_agent = gpio_agent::type_id::create("irq_agent", this);

    uvm_config_int::set(this, "one_wire_agent", "is_active",
                        cfg.one_wire_active);
    one_wire_agent_cfg =
        one_wire_slave_agent_cfg::type_id::create("one_wire_agent_cfg");
    one_wire_agent_cfg.coverage = cfg.one_wire_coverage;
    one_wire_agent_cfg.vif = cfg.one_wire_vif;
    uvm_config_db#(one_wire_slave_agent_cfg)::set(this, "one_wire_agent", "cfg",
                                                  one_wire_agent_cfg);
    one_wire_agent =
        one_wire_slave_agent::type_id::create("one_wire_agent", this);

    scoreboard =
        one_wire_master_scoreboard::type_id::create("scoreboard", this);

    reg_block =
        one_wire_master_reg_block::type_id::create("reg_block", this);
    reg_block.configure();
    reg_block.build();
    if (reg_block.set_coverage(cfg.reg_coverage) != cfg.reg_coverage) begin
      `uvm_warning(get_type_name(), "Failed to set register coverage")
    end
    reg_block.lock_model();

    reg_adapter = apb_reg_adapter::type_id::create("reg_adapter", this);

    reg_predictor = uvm_reg_predictor#(apb_sequence_item)::type_id::create(
        "reg_predictor", this);
  endfunction

  virtual function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    apb_agent.monitor.seq_item_port.connect(scoreboard.apb_in);
    apb_agent.monitor.seq_item_port.connect(reg_predictor.bus_in);
    irq_agent.monitor.seq_item_port.connect(scoreboard.irq_in);
    one_wire_agent.monitor.sample_port.connect(scoreboard.one_wire_in);
    one_wire_agent.monitor.drive_port.connect(scoreboard.drive_in);
    one_wire_agent.monitor.spu_port.connect(scoreboard.spu_in);
    scoreboard.ctrl = reg_block.ctrl;
    reg_predictor.map = reg_block.default_map;
    reg_predictor.adapter = reg_adapter;
    reg_block.default_map.set_sequencer(apb_agent.sequencer, reg_adapter);
  endfunction
endclass
