// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_operation_sequence extends uvm_reg_sequence;
  `uvm_object_utils(one_wire_master_operation_sequence)

  bit [7:0] clk_div;
  rand bit [7:0] param;
  bit [1:0] operation;
  rand bit overdrive;
  rand bit spu;
  rand bit apu;
  rand bit irq_en;
  bit [7:0] result;

  function new(string name = "one_wire_master_operation_sequence");
    super.new(name);
  endfunction

  virtual task body();
    one_wire_master_reg_block model;
    uvm_status_e status;

    if (this.model == null) begin
      `uvm_error(get_type_name(), "Model is null")
      return;
    end

    if (!$cast(model, this.model)) begin
      `uvm_error(get_type_name(),
                 "Failed to cast model to one_wire_master_env::reg_block")
      return;
    end

    model.clk_div.set(clk_div);
    update_reg(model.clk_div, status);
    if (status != UVM_IS_OK) begin
      `uvm_error(get_type_name(), "Failed to update clk_div register")
      return;
    end

    model.param.set(param);
    update_reg(model.param, status);
    if (status != UVM_IS_OK) begin
      `uvm_error(get_type_name(), "Failed to update param register")
      return;
    end

    model.ctrl.operation_en.set(1);
    model.ctrl.operation.set(operation);
    model.ctrl.overdrive.set(overdrive);
    model.ctrl.spu.set(spu);
    model.ctrl.apu.set(apu);
    model.ctrl.irq_en.set(irq_en);
    model.ctrl.irq.set(0);
    update_reg(model.ctrl, status);
    if (status != UVM_IS_OK) begin
      `uvm_error(get_type_name(), "Failed to update ctrl register")
      return;
    end

    do begin
      #1us mirror_reg(model.ctrl, status);
      if (status != UVM_IS_OK) begin
        `uvm_error(get_type_name(), "Failed to mirror ctrl register")
        return;
      end
    end while (model.ctrl.operation_en.get());

    if (model.ctrl.operation.get() != operation) begin
      `uvm_error(get_type_name(), "Unexpected operation value")
    end

    if (model.ctrl.overdrive.get() != overdrive) begin
      `uvm_error(get_type_name(), "Unexpected overdrive value")
    end

    if (model.ctrl.spu.get() != spu) begin
      `uvm_error(get_type_name(), "Unexpected spu value")
    end

    if (model.ctrl.apu.get() != apu) begin
      `uvm_error(get_type_name(), "Unexpected apu value")
    end

    if (model.ctrl.irq_en.get() != irq_en) begin
      `uvm_error(get_type_name(), "Unexpected irq_en value")
    end

    if (!model.ctrl.irq.get()) begin
      `uvm_error(get_type_name(), "Unexpected irq value")
    end

    mirror_reg(model.param, status);
    if (status != UVM_IS_OK) begin
      `uvm_error(get_type_name(), "Failed to mirror param register")
      return;
    end

    if (model.param.get() != param) begin
      `uvm_error(get_type_name(), "Unexpected param value")
    end

    mirror_reg(model.clk_div, status);
    if (status != UVM_IS_OK) begin
      `uvm_error(get_type_name(), "Failed to mirror clk_div register")
      return;
    end

    if (model.clk_div.get() != clk_div) begin
      `uvm_error(get_type_name(), "Unexpected clk_div value")
    end

    mirror_reg(model.result, status);
    if (status != UVM_IS_OK) begin
      `uvm_error(get_type_name(), "Failed to mirror result register")
      return;
    end

    if (model.result.get() != result) begin
      `uvm_error(get_type_name(), "Unexpected result value")
    end
  endtask
endclass
