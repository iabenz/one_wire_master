// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_env_cfg extends uvm_object;
  `uvm_object_utils(one_wire_master_env_cfg)

  uvm_active_passive_enum apb_active = UVM_ACTIVE;
  virtual apb_agent_if apb_vif;
  realtime apb_pclk_period;

  bit irq_coverage = 1;
  virtual gpio_agent_if irq_vif;

  uvm_active_passive_enum one_wire_active = UVM_ACTIVE;
  bit one_wire_coverage = 1;
  virtual one_wire_agent_if one_wire_vif;

  uvm_reg_cvr_t reg_coverage = UVM_CVR_ADDR_MAP | UVM_CVR_FIELD_VALS;

  function new(string name = "one_wire_master_env_cfg");
    super.new(name);
  endfunction
endclass
