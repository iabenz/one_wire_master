// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

`uvm_analysis_imp_decl(_apb)
`uvm_analysis_imp_decl(_irq)
`uvm_analysis_imp_decl(_one_wire)
`uvm_analysis_imp_decl(_drive)
`uvm_analysis_imp_decl(_spu)

class one_wire_master_scoreboard extends uvm_scoreboard;
  `uvm_component_utils(one_wire_master_scoreboard)

  uvm_analysis_imp_apb#(apb_sequence_item, one_wire_master_scoreboard) apb_in;
  uvm_analysis_imp_irq#(gpio_sequence_item, one_wire_master_scoreboard) irq_in;
  uvm_analysis_imp_one_wire#(one_wire_sequence_item, one_wire_master_scoreboard)
      one_wire_in;
  uvm_analysis_imp_drive#(gpio_sequence_item, one_wire_master_scoreboard)
      drive_in;
  uvm_analysis_imp_spu#(gpio_sequence_item, one_wire_master_scoreboard) spu_in;
  one_wire_master_ctrl_reg ctrl;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    apb_in = new("apb_in", this);
    irq_in = new("irq_in", this);
    one_wire_in = new("one_wire_in", this);
    drive_in = new("drive_in", this);
    spu_in = new("spu_in", this);
  endfunction

  virtual function void write_apb(apb_sequence_item seq_item);
    `uvm_info(get_type_name(),
              $sformatf("APB address %h %0s %h", seq_item.addr,
                        seq_item.write ? "write" : "read", seq_item.data),
              UVM_HIGH)
  endfunction

  virtual function void write_irq(gpio_sequence_item seq_item);
    `uvm_info(get_type_name(), {"IRQ ", seq_item.value ? "set" : "cleared"},
              UVM_MEDIUM)

    if (seq_item.value && !ctrl.irq_en.get_mirrored_value()) begin
      `uvm_error(get_type_name(), "IRQ set when not enabled")
    end

    if (!seq_item.value && ctrl.irq.get_mirrored_value()) begin
      `uvm_error(get_type_name(), "IRQ cleared unexpectedly")
    end
  endfunction

  local function void print_one_wire(one_wire_sequence_item seq_item);
    string message = "1-Wire";

    if (seq_item.overdrive) begin
      message = {message, " overdrive"};
    end

    if (seq_item.reset) begin
      message = {message, " reset"};
      if (seq_item.data) begin
        message = {message, " presence"};
      end
    end else begin
      message = {message, $sformatf(" %b bit", seq_item.data)};
    end

    `uvm_info(get_type_name(), message, UVM_MEDIUM)
  endfunction

  virtual function void write_one_wire(one_wire_sequence_item seq_item);
    print_one_wire(seq_item);

    if (!ctrl.operation_en.get_mirrored_value()) begin
      `uvm_error(get_type_name(), "Unexpected 1-Wire operation")
    end

    if (seq_item.reset !=
        (ctrl.operation.get_mirrored_value() ==
         one_wire_master_bus_reset_operation)) begin
      `uvm_error(get_type_name(), "Unexpected 1-Wire reset value")
    end

    if (seq_item.overdrive != ctrl.overdrive.get_mirrored_value()) begin
      `uvm_error(get_type_name(), "Unexpected 1-Wire overdrive value")
    end
  endfunction

  virtual function void write_drive(gpio_sequence_item seq_item);
    `uvm_info(get_type_name(), {"Drive ", seq_item.value ? "set" : "cleared"},
              UVM_HIGH)

    if (seq_item.value && !ctrl.operation_en.get_mirrored_value()) begin
      `uvm_error(get_type_name(), "Drive set when not enabled")
    end
  endfunction

  virtual function void write_spu(gpio_sequence_item seq_item);
    `uvm_info(get_type_name(), {"SPU ", seq_item.value ? "set" : "cleared"},
              UVM_HIGH)

    if (seq_item.value &&
        !(ctrl.spu.get_mirrored_value() ||
          (ctrl.apu.get_mirrored_value() &&
           ctrl.operation_en.get_mirrored_value()))) begin
      `uvm_error(get_type_name(), "SPU set when not enabled")
    end
  endfunction
endclass
