// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

`include <uvm_macros.svh>

package one_wire_master_env_pkg;
  import apb_agent_pkg::*;
  import gpio_agent_pkg::*;
  import one_wire_agent_pkg::*;
  import uvm_pkg::*;

  localparam bit [1:0] one_wire_master_bus_reset_operation = 0,
                       one_wire_master_write_bit_operation = 1,
                       one_wire_master_write_byte_operation = 2,
                       one_wire_master_triplet_operation = 3;

  `include "one_wire_master_ctrl_reg_coverage.svh"
  `include "one_wire_master_ctrl_reg.svh"
  `include "one_wire_master_param_reg.svh"
  `include "one_wire_master_result_reg.svh"
  `include "one_wire_master_clk_div_reg.svh"
  `include "one_wire_master_locked_reg_cbs.svh"
  `include "one_wire_master_reg_block_coverage.svh"
  `include "one_wire_master_reg_block.svh"
  `include "one_wire_master_scoreboard.svh"
  `include "one_wire_master_env_cfg.svh"
  `include "one_wire_master_env.svh"
  `include "one_wire_master_operation_sequence.svh"
endpackage
