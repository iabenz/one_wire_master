// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_locked_reg_cbs extends uvm_reg_cbs;
  `uvm_object_utils(one_wire_master_locked_reg_cbs)

  uvm_reg_field lock_field;

  function new(string name = "one_wire_master_locked_reg_cbs");
    super.new(name);
  endfunction

  virtual function void post_predict(
      input uvm_reg_field fld, input uvm_reg_data_t previous,
      inout uvm_reg_data_t value, input uvm_predict_e kind,
      input uvm_path_e path, input uvm_reg_map map);
    if (kind == UVM_PREDICT_WRITE && lock_field.get_mirrored_value()) begin
      value = previous;
    end
  endfunction
endclass
