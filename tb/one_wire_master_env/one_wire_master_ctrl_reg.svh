// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_ctrl_reg extends uvm_reg;
  `uvm_object_utils(one_wire_master_ctrl_reg)

  rand uvm_reg_field operation_en;
  rand uvm_reg_field operation;
  rand uvm_reg_field overdrive;
  rand uvm_reg_field spu;
  rand uvm_reg_field apu;
  rand uvm_reg_field irq_en;
  rand uvm_reg_field irq;

  local one_wire_master_ctrl_reg_coverage coverage;

  function new(string name = "one_wire_master_ctrl_reg");
    super.new(name, .n_bits(8), .has_coverage(UVM_CVR_FIELD_VALS));
  endfunction

  virtual function void build();
    operation_en = uvm_reg_field::type_id::create("operation_en");
    operation_en.configure(.parent(this), .size(1), .lsb_pos(7), .access("W1S"),
                           .volatile(1), .reset(0), .has_reset(1), .is_rand(1),
                           .individually_accessible(0));

    operation = uvm_reg_field::type_id::create("operation");
    operation.configure(.parent(this), .size(2), .lsb_pos(5), .access("RW"),
                        .volatile(0), .reset(0), .has_reset(1), .is_rand(1),
                        .individually_accessible(0));

    overdrive = uvm_reg_field::type_id::create("overdrive");
    overdrive.configure(.parent(this), .size(1), .lsb_pos(4), .access("RW"),
                        .volatile(0), .reset(0), .has_reset(1), .is_rand(1),
                        .individually_accessible(0));

    spu = uvm_reg_field::type_id::create("spu");
    spu.configure(.parent(this), .size(1), .lsb_pos(3), .access("RW"),
                  .volatile(0), .reset(0), .has_reset(1), .is_rand(1),
                  .individually_accessible(0));

    apu = uvm_reg_field::type_id::create("apu");
    apu.configure(.parent(this), .size(1), .lsb_pos(2), .access("RW"),
                  .volatile(0), .reset(0), .has_reset(1), .is_rand(1),
                  .individually_accessible(0));

    irq_en = uvm_reg_field::type_id::create("irq_en");
    irq_en.configure(.parent(this), .size(1), .lsb_pos(1), .access("RW"),
                     .volatile(0), .reset(0), .has_reset(1), .is_rand(1),
                     .individually_accessible(0));

    irq = uvm_reg_field::type_id::create("irq");
    irq.configure(.parent(this), .size(1), .lsb_pos(0), .access("W0C"),
                  .volatile(1), .reset(0), .has_reset(1), .is_rand(1),
                  .individually_accessible(0));
  endfunction

  protected virtual function void sample(uvm_reg_data_t data,
                                         uvm_reg_data_t byte_en, bit is_read,
                                         uvm_reg_map map);
    if (get_coverage(UVM_CVR_FIELD_VALS)) begin
      if (coverage == null) begin
        coverage = new;
      end
      coverage.sample(data);
    end
  endfunction
endclass
