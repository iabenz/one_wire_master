// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_detect_short_test extends test;
  `uvm_component_utils(one_wire_detect_short_test)

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    env_cfg.one_wire_active = UVM_PASSIVE;
  endfunction

  virtual task run_phase(uvm_phase phase);
    one_wire_master_operation_sequence run_operation =
        one_wire_master_operation_sequence::type_id::create("run_operation");
    super.run_phase(phase);
    phase.raise_objection(this);
    env_cfg.one_wire_vif.sample <= 0;
    env_cfg.one_wire_vif.apu <= 0;
    run_operation.model = env.reg_block;
    run_operation.clk_div = clk_div;
    run_operation.operation = one_wire_master_bus_reset_operation;
    run_operation.result = 'b11;
    if (!run_operation.randomize()) begin
      `uvm_error(get_type_name(), "Failed to randomize sequence")
    end
    env.one_wire_agent.monitor.overdrive = run_operation.overdrive;
    run_operation.start(env.one_wire_agent.sequencer);
    phase.drop_objection(this);
  endtask
endclass
