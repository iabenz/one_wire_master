// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

`include <uvm_macros.svh>

package tests_pkg;
  import one_wire_master_env_pkg::*;
  import sequences_pkg::*;
  import uvm_pkg::*;

  `include "test_cfg.svh"
  `include "test.svh"
  `include "check_registers_test.svh"
  `include "one_wire_bus_reset_test.svh"
  `include "one_wire_detect_short_test.svh"
  `include "one_wire_touch_bit_test.svh"
  `include "one_wire_write_bit_test.svh"
  `include "one_wire_read_bit_test.svh"
  `include "one_wire_touch_byte_test.svh"
  `include "one_wire_write_byte_test.svh"
  `include "one_wire_read_byte_test.svh"
  `include "one_wire_triplet_test.svh"
endpackage
