// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Module: one_wire_master_timer
//
// Timer with fractional clock divider.
//
// Ports:
//
//   clk_div - Divides the clock by clk_div / 4 + 1, targeting 4MHz for correct
//             1-Wire timing. Must remain valid while the timer is active.
//
//   count - Time count in 0.25us increments.
module one_wire_master_timer (
  input logic clk,
  input logic reset,
  input logic [7:0] clk_div,
  output logic [12:0] count
);
  logic [6:0] divide_count;
  logic [1:0] fraction_count;

  always_ff @(posedge clk or posedge reset) begin
    if (reset) begin
      count <= '0;
      divide_count <= '0;
      fraction_count <= '0;
    end else if (divide_count ==
                 clk_div[7:2] +
                     (fraction_count < clk_div[1:0] ? 6'd1 : 6'd0)) begin
      count <= count + 13'd1;
      divide_count <= '0;
      fraction_count <= fraction_count + 2'd1;
    end else begin
      divide_count <= divide_count + 7'd1;
    end
  end
endmodule
