// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Module: one_wire_master_top
//
// AMBA 3 APB to 1-Wire master peripheral features standard and overdrive
// speeds, strong pull-up (SPU), and active pull-up (APU).
//
// Ports:
//
//   pclk - Divider must be set to generate a 4MHz output prior to starting a
//          1-Wire operation.
//
//   preset_n - Active-low, asynchronous reset.
//
//   paddr - Address to access.
//
//   psel - Select this slave for transfers.
//
//   penable - Enable write and read transfers.
//
//   pwrite - Set for a write transfer or clear for a read transfer.
//
//   pwdata - Data written on a write transfer.
//
//   pready - Asserted to indicate that a transfer is complete.
//
//   prdata - Data read on a read transfer.
//
//   irq - Asserted to indicate that an interrupt request is pending.
//
//   sample - Sample the 1-Wire bus.
//
//   apu - Sample the 1-Wire bus at a lower threshold for active pull-up. May be
//         tied to the sample port if a separate threshold is not available.
//
//   drive - Drive the 1-Wire bus low.
//
//   spu - Drive the 1-Wire bus high with strong pull-up.
module one_wire_master_top (
  // APB interface
  input logic pclk,
  input logic preset_n,
  input logic [1:0] paddr,
  input logic psel,
  input logic penable,
  input logic pwrite,
  input logic [7:0] pwdata,
  output logic pready,
  output logic [7:0] prdata,

  output logic irq,

  // 1-Wire interface
  input logic sample,
  input logic apu,
  output logic drive,
  output logic spu
);
  assign pready = '1;

  // Peripheral and controller interfaces
  wire logic clk = pclk;
  wire logic reset = ~preset_n;

  // Controller interface
  logic [7:0] clk_div;
  logic start;
  logic [1:0] operation;
  logic overdrive;
  logic spu_en;
  logic apu_en;
  logic [7:0] param;
  logic done;
  logic [7:0] result;

  one_wire_master_memory memory (
    // Peripheral interface
    .enable(psel & ~penable),
    .write(pwrite),
    .addr(paddr),
    .wdata(pwdata),
    .rdata(prdata),
    .*
  );

  one_wire_master_controller controller (.*);
endmodule
