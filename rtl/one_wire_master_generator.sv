// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Module: one_wire_master_generator
//
// Manages the 1-Wire interface to perform bus reset and write bit operations.
//
// Ports:
//
//   clk_div - 1-Wire clock divider. Must remain valid for the entire operation.
//
//   start - Start an operation when the generator is idle.
//
//   bus_reset - Set to select bus reset or clear to select write bit.
//               Must remain valid while start is set.
//
//   overdrive - Set to select overdrive timing or clear to select standard
//               timing. Must remain valid for the entire operation.
//
//   spu_en - Enable SPU when the generator is idle.
//
//   apu_en - Enable APU. Must remain valid for the entire operation.
//
//   param - Value to transmit when write bit is selected. Must remain valid
//           while start is set.
//
//   done - Asserted for one clock cyle to indicate that the operation is done
//          and the generator is idle.
//
//   result - For bus reset, bit 0 indicates presence detected and bit 1
//            indicates short detected. For write bit, bit 0 indicates the
//            sampled value. Only valid when the operation is done and the
//            generator is idle.
module one_wire_master_generator (
  input logic clk,
  input logic reset,
  input logic [7:0] clk_div,
  input logic start,
  input logic bus_reset,
  input logic overdrive,
  input logic spu_en,
  input logic apu_en,
  input logic param,
  output logic done,
  output logic [1:0] result,

  // 1-Wire interface
  input logic sample,
  input logic apu,
  output logic drive,
  output logic spu
);
  // 1-Wire timing in 0.25us increments.
  localparam bit [12:0] tRSTL_STD = 2240,
                        tRSTL_OD = 224,
                        tSI_STD = 32,
                        tSI_OD = 3,
                        tMSP_STD = 272,
                        tMSP_OD = 32,
                        tRSTH_STD = 2240,
                        tRSTH_OD = 224,
                        tW0L_STD = 256,
                        tW0L_OD = 32,
                        tW1L_STD = 32,
                        tW1L_OD = 3,
                        tMSR_STD = 48,
                        tMSR_OD = 7,
                        tSLOT_STD = 277,
                        tSLOT_OD = 53,
                        tAPU_STD = 10,
                        tAPU_OD = 2,
                        tRSTL_APU_STD = tRSTL_STD + tAPU_STD,
                        tRSTL_APU_OD = tRSTL_OD + tAPU_OD,
                        tRSTL_SI_STD = tRSTL_STD + tSI_STD,
                        tRSTL_SI_OD = tRSTL_OD + tSI_OD,
                        tRSTL_MSP_STD = tRSTL_STD + tMSP_STD,
                        tRSTL_MSP_OD = tRSTL_OD + tMSP_OD,
                        tRSTL_RSTH_STD = tRSTL_STD + tRSTH_STD,
                        tRSTL_RSTH_OD = tRSTL_OD + tRSTH_OD,
                        tW1L_APU_STD = tW1L_STD + tAPU_STD,
                        tW1L_APU_OD = tW1L_OD + tAPU_OD;

  logic sample_sync;
  logic apu_sync;
  logic timer_reset;

  logic [12:0] timer_count,
               tRSTL,
               tRSTL_APU,
               tRSTL_SI,
               tRSTL_MSP,
               tRSTL_RSTH,
               tMSR,
               tW0L,
               tW1L,
               tW1L_APU,
               tSLOT,
               timer_threshold;

  logic timer_done;

  enum logic [3:0] {
    idle,
    bus_reset_low,
    bus_reset_apu,
    bus_reset_short_detect,
    bus_reset_presence_detect,
    bus_reset_presence_apu,
    bus_reset_recover,
    write_0_bit_sample,
    write_0_bit_low,
    write_1_bit_low,
    write_1_bit_apu,
    write_1_bit_sample,
    write_bit_apu,
    write_bit_recover
  } state;

  one_wire_master_synchronizer sample_synchronizer (
    .in(sample),
    .out(sample_sync),
    .*
  );

  one_wire_master_synchronizer apu_synchronizer (
    .in(apu),
    .out(apu_sync),
    .*
  );

  one_wire_master_timer timer (
    .reset(timer_reset),
    .count(timer_count),
    .*
  );

  assign tRSTL = overdrive ? tRSTL_OD : tRSTL_STD;
  assign tRSTL_APU = overdrive ? tRSTL_APU_OD : tRSTL_APU_STD;
  assign tRSTL_SI = overdrive ? tRSTL_SI_OD : tRSTL_SI_STD;
  assign tRSTL_MSP = overdrive ? tRSTL_MSP_OD : tRSTL_MSP_STD;
  assign tRSTL_RSTH = overdrive ? tRSTL_RSTH_OD : tRSTL_RSTH_STD;
  assign tMSR = overdrive ? tMSR_OD : tMSR_STD;
  assign tW0L = overdrive ? tW0L_OD : tW0L_STD;
  assign tW1L = overdrive ? tW1L_OD : tW1L_STD;
  assign tW1L_APU = overdrive ? tW1L_APU_OD : tW1L_APU_STD;
  assign tSLOT = overdrive ? tSLOT_OD : tSLOT_STD;

  always_comb begin
    unique case (state)
      bus_reset_low: timer_threshold = tRSTL;
      bus_reset_apu: timer_threshold = tRSTL_APU;
      bus_reset_short_detect: timer_threshold = tRSTL_SI;
      bus_reset_presence_detect: timer_threshold = tRSTL_MSP;
      bus_reset_presence_apu, bus_reset_recover: timer_threshold = tRSTL_RSTH;
      write_0_bit_sample, write_1_bit_sample: timer_threshold = tMSR;
      write_0_bit_low: timer_threshold = tW0L;
      write_1_bit_low: timer_threshold = tW1L;
      write_1_bit_apu: timer_threshold = tW1L_APU;
      write_bit_apu, write_bit_recover: timer_threshold = tSLOT;
      default: timer_threshold = '0;
    endcase
  end

  assign timer_done = timer_count == timer_threshold;

  always_ff @(posedge clk or posedge reset) begin
    done <= '0;
    drive <= '0;
    spu <= '0;

    if (reset) begin
      timer_reset <= '1;
      state <= idle;
    end else begin
      timer_reset <= '0;

      unique case (state)
        idle: begin
          if (start) begin
            if (bus_reset) begin
              state <= bus_reset_low;
            end else if (param) begin
              state <= write_1_bit_low;
            end else begin
              state <= write_0_bit_sample;
            end
          end else begin
            // If SPU is enabled, activate SPU while no short is detected.
            spu <= spu_en & sample_sync;
            timer_reset <= '1;
          end
        end

        bus_reset_low: begin
          // Drive bus low until tRSTL.
          if (timer_done) begin
            state <= bus_reset_apu;
          end else begin
            drive <= '1;
          end
        end

        bus_reset_apu: begin
          // If APU is enabled, activate SPU once APU threshold reached until
          // tRSTL + tAPU.
          if (timer_done) begin
            state <= bus_reset_short_detect;
          end else begin
            spu <= apu_en & apu_sync;
          end
        end

        bus_reset_short_detect: begin
          // Wait until tRSTL + tSI to detect short.
          if (timer_done) begin
            state <= bus_reset_presence_detect;
          end
        end

        bus_reset_presence_detect: begin
          // Wait until tRSTL + tMSP to detect presence.
          if (timer_done) begin
            state <= bus_reset_presence_apu;
          end
        end

        bus_reset_presence_apu: begin
          // If APU is enabled, activate SPU once APU threshold reached.
          spu <= apu_en & apu_sync;

          // Wait until sample threshold is reached or tRSTL + tRSTH to indicate
          // done.
          if (timer_done) begin
            done <= '1;
            timer_reset <= '1;
            state <= idle;
          end else if (sample_sync) begin
            state <= bus_reset_recover;
          end
        end

        bus_reset_recover: begin
          // If APU is enabled, activate SPU while no short is detected.
          spu <= apu_en & sample_sync;

          // Wait until tRSTL + tRSTH to indicate done.
          if (timer_done) begin
            done <= '1;
            timer_reset <= '1;
            state <= idle;
          end
        end

        write_0_bit_sample: begin
          // Drive bus low.
          drive <= '1;

          // Wait until tMSR to sample.
          if (timer_done) begin
            state <= write_0_bit_low;
          end
        end

        write_0_bit_low: begin
          // Drive bus low until tW0L.
          if (timer_done) begin
            state <= write_bit_recover;
          end else begin
            drive <= '1;
          end
        end

        write_1_bit_low: begin
          // Drive bus low until tW1L.
          if (timer_done) begin
            state <= write_1_bit_apu;
          end else begin
            drive <= '1;
          end
        end

        write_1_bit_apu: begin
          // If APU is enabled, activate SPU once APU threshold reached until
          // tW1L + tAPU.
          if (timer_done) begin
            state <= write_1_bit_sample;
          end else begin
            spu <= apu_en & apu_sync;
          end
        end

        write_1_bit_sample: begin
          // Wait until tMSR to sample.
          if (timer_done) begin
            state <= write_bit_apu;
          end
        end

        write_bit_apu: begin
          // If APU is enabled, activate SPU once APU threshold reached.
          spu <= apu_en & apu_sync;

          // Wait until sample threshold is reached or tSLOT to indicate done.
          if (timer_done) begin
            done <= '1;
            timer_reset <= '1;
            state <= idle;
          end else if (sample_sync) begin
            state <= write_bit_recover;
          end
        end

        write_bit_recover: begin
          // If APU is enabled, activate SPU while no short is detected.
          spu <= apu_en & sample_sync;

          // Wait until tSLOT to indicate done.
          if (timer_done) begin
            done <= '1;
            timer_reset <= '1;
            state <= idle;
          end
        end
      endcase
    end
  end

  always_ff @(posedge clk) begin
    unique0 case (state)
      bus_reset_short_detect: result[1] <= ~sample;
      bus_reset_presence_detect: result[0] <= ~sample;
      write_0_bit_sample, write_1_bit_sample: result[0] <= sample;
    endcase
  end
endmodule
