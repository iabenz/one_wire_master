1-Wire Master
=============

AMBA 3 APB to 1-Wire master peripheral implemented in SystemVerilog features
standard and overdrive speeds, strong pull-up (SPU), and active pull-up (APU).
The RTL code has no dependencies beyond SystemVerilog IEEE 1800-2012 support.
The testbench requires UVM 1.2, and support for simulation using Xilinx Vivado
2021.1 or later and GNU Make is included.
Validation was performed on a Xilinx Artix-7 FPGA with the 1-Wire master,
utilizing 94 slice LUTs and 92 slice registers, connected to a MicroBlaze CPU.

The project is organized using the following directories:
- `rtl` - RTL design files
- `sim` - Simulation with Xilinx Vivado tools
- `tb` - Testbench files including the top module (tb), tests, and reusable UVM
  agent and environment (env) components

Simulation
----------

Simulations may be run by invoking `make` from the `sim` directory with targets:
- `elab` - Elaborate the testbench.
- `sim` - Run a simulation non-interactively in the terminal.
- `gui_sim` - Run a simulation interactively in Vivado.
- `coverage` - Generate a functional coverage report.
- `clean` - Delete all generated files.

The `sim` and `gui_sim` targets should use the `TEST` and optional `SEED`
parameters to select a simulation.
The available tests are listed in the `tb/tests` directory.

    make elab
    make sim TEST=one_wire_write_byte_test
    make gui_sim TEST=one_wire_read_byte_test SEED=2000

Registers
---------

There are four byte-sized, APB-accessible registers:
- `CTRL` (address 0) - Packed control fields:
  - `OPERATION_EN` (bit 7) -
    Set to start an operation.
    Cleared automatically when the operation is complete.
  - `OPERATION` (bits 5-6) -
    Select bus reset (0), write bit (1), write byte (2), or triplet (3).
  - `OVERDRIVE` (bit 4) -
    Set to select overdrive timing or clear to select standard timing.
  - `SPU` (bit 3) -
    Set to enable strong pull-up when no operation is in progress.
  - `APU` (bit 2) - Set to enable active pull-up.
  - `IRQ_EN` (bit 1) - Enable interrupt output.
  - `IRQ` (bit 0) - Interrupt is set when the last operation is complete.
- `PARAM` (address 1) -
  Value to transmit when write bit, write byte, or triplet is selected.
  Only bit 0 is used for write bit and triplet.
- `RESULT` (address 2) -
  Read-only result of the last operation.
  For bus reset, bit 0 indicates presence detected and bit 1 indicates short
  detected.
  For write bit, bit 0 indicates the sampled value.
  For triplet, bit 2 indicates the read bit, bit 1 indicates the read complement
  (second) bit, and bit 0 indicates the written bit.
- `CLK_DIV` (address 3) -
  Divides PCLK by `CLK_DIV / 4 + 1`, targeting 4MHz for correct 1-Wire timing.

Only `IRQ` and `IRQ_EN` are writable when `OPERATION_EN` is set.
All registers reset to zero.
